//
//  ContentView.swift
//  HelloWorld
//
//  Created by Mohamed Ben Hajla on 13.08.20.
//  Copyright © 2020 Mohamed Ben Hajla. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
         Text("Hello Standalone iOS!")
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
