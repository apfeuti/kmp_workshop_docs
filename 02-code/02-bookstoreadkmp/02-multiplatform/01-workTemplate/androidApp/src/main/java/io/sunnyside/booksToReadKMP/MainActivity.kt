package io.sunnyside.booksToReadKMP

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.compose.Composable
import androidx.ui.livedata.observeAsState
import androidx.ui.core.Modifier
import androidx.ui.core.setContent
import androidx.ui.foundation.Text
import androidx.ui.layout.Column
import androidx.ui.material.MaterialTheme
import androidx.ui.tooling.preview.Preview
import io.sunnyside.booksToReadKMP.ui.AppTheme
import io.sunnyside.common.bookstoreadkmp.BookRepository
import io.sunnyside.common.bookstoreadkmp.Book
import androidx.ui.layout.padding
import androidx.ui.unit.dp

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val bookRepository: BookRepository = BookRepository()
        val bookViewModel: BookViewModel = BookViewModel(bookRepository)
        setContent {
            mainLayout(bookViewModel)
        }
    }
}

@Composable
fun mainLayout(bookViewModel: BookViewModel) {
    MaterialTheme {

        val books =  bookViewModel.booksData.observeAsState(emptyList())

        Column {
            books?.value?.forEach() { book ->
                Row(book)
            }
        }
    }
}

@Composable
fun Row(book: Book) {
    Column(
            modifier = Modifier.padding(16.dp)
    ) {
        Text("Book:")
        Text(text = "${book.bookCategory} (${book.title})")

    }

}

@Preview(showBackground = true)
@Composable
fun DefaultPreview() {
    AppTheme {
        Text("Android")
    }
}
