//
//  BookApiService.swift
//  BooksToRead
//
//  Created by Mohamed Ben Hajla on 13.08.20.
//  Copyright © 2020 Mohamed Ben Hajla. All rights reserved.
//

import Foundation
import Combine


import Foundation

/// The `Result` of an operation.
public enum Result<T> {

    /// In case of `.success`, an associated value of type `<T>` is returned.
    case success(T)

    /// In case of `.failure`, an error `` is returned.
    case failure(Error)
}

enum DataError: String, Error {
    case noData = "data not available"
    case noNetwork = "network not avalilable"
    case noDetails = "Error"
}

class BookApiService {
    
  
    func fetchBooksWithCallBack(completion: @escaping (Result<[Book]?>) -> Void ){
        
        let urlString:String = "http://localhost:3010/books"
        let url = URL(string: urlString)!

        let task = URLSession.shared.dataTask(with: url, completionHandler: { data, response, error in
            if let data = data, let model = try? JSONDecoder().decode([Book].self, from: data) {
                completion(.success(model))
            } else {
                completion(.failure(error ?? DataError.noDetails))
            }
        })

        task.resume()
        
    }
  
    
}
