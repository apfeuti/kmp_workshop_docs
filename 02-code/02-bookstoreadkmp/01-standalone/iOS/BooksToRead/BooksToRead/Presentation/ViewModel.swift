//
//  ViewModel.swift
//  BooksToRead
//
//  Created by Mohamed Ben Hajla on 13.08.20.
//  Copyright © 2020 Mohamed Ben Hajla. All rights reserved.
//

import Foundation
import Combine
import SwiftUI

class ViewModel:ObservableObject{
    let bookAPIService:BookApiService =  BookApiService()
    @Published var books = [Book]()
    @State var cancelable: AnyCancellable? = nil
    
    func fetchBooks(){
        
        bookAPIService.fetchBooksWithCallBack() { result in
            switch result {
            case .success(let data):
                do {
                    guard let data = data else {
                        throw DataError.noDetails
                    }
                    
                    DispatchQueue.main.async {
                        if (data.count > 0){
                            for item in data {
                                self.books.append(item)
                            }
                        }
                    }
                    
                } catch {
                    print("Error")
                }
            case .failure(let error):
                print("Error while retrieveing user info: \(error)")
                DispatchQueue.main.async {
                    
                }
            }
        }
    }
    
    
}
