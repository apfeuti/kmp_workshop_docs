import React, { useState, useEffect } from "react";
import logo from './logo.svg';
import './App.css';
import shared from 'HelloKMP-shared';

function App() {
 const [greeting, setGreeting] = useState("")
 var greet = shared.io.sunnyside.hellokmp.CommonGreeter.prototype.greet

  return (
    <div className="App">
      <h1>React HelloWorld App</h1>

      <p> {greeting} </p>
     <p> 
      <button onClick={() => setGreeting(greet())}>
        Greet
      </button>
     </p>
    </div>
  );
}

export default App;
