(function (_, Kotlin) {
  'use strict';
  var Kind_CLASS = Kotlin.Kind.CLASS;
  function CommonGreeter() {
  }
  CommonGreeter.prototype.greet = function () {
    return 'Hello from Kmp';
  };
  CommonGreeter.$metadata$ = {
    kind: Kind_CLASS,
    simpleName: 'CommonGreeter',
    interfaces: []
  };
  var package$io = _.io || (_.io = {});
  var package$sunnyside = package$io.sunnyside || (package$io.sunnyside = {});
  var package$hellokmp = package$sunnyside.hellokmp || (package$sunnyside.hellokmp = {});
  package$hellokmp.CommonGreeter = CommonGreeter;
  Kotlin.defineModule('HelloKMP-shared', _);
  return _;
}(module.exports, require('kotlin')));

//# sourceMappingURL=HelloKMP-shared.js.map
