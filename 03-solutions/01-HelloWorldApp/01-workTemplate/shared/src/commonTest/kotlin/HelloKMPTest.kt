import io.sunnyside.hellokmp.CommonGreeter
import kotlin.test.Test
import kotlin.test.assertEquals


open class HelloKMPTest{

    @Test
    open fun greetKMPTest(){
        val commonGreeter = CommonGreeter()
        assertEquals(
            "Hello from Kmp",
            commonGreeter.greet()
        )
    }

}