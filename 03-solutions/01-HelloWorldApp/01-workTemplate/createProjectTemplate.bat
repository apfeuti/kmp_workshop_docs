

rem ***************Folders ************************

rem create top level folders for android, ios and shared subprojects
mkdir  shared
mkdir  androidApp
mkdir  iosApp
mkdir  jsApp

rem ********** shared subproject *****************************

rem create  Sub-folders for the shared project
mkdir  shared\src\commonMain\kotlin
mkdir  shared\src\jvmMain\kotlin
mkdir  shared\src\iosMain\kotlin
mkdir  shared\src\jsMain\kotlin
mkdir  shared\src\main


rem ******** jsApp subproject *******************************

rem create  Sub-folders for the jsApp project

mkdir  jsApp\src\main\kotlin
mkdir  jsApp\src\main\resources
mkdir  jsApp\src\main\kotlin\io.sunnyside.hellokmp


rem  Create the source Files and the build gradle File

copy /y nul shared\src\commonMain\kotlin\common.kt
copy /y nul shared\src\jvmMain\kotlin\android.kt
copy /y nul shared\src\iosMain\kotlin\ios.kt
copy /y nul shared\src\jsMain\kotlin\js.kt
copy /y nul shared\src\main\AndroidManifest.xml
copy /y nul shared\build.gradle 

rem  Create the source Files and the build gradle File

copy /y nul jsApp\src\main\kotlin\io.sunnyside.hellokmp\main.kt
copy /y nul jsApp\src\main\resources\index.html
copy /y nul jsApp\src\main\resources\styles.css
copy /y nul jsApp\build.gradle
