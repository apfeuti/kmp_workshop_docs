package io.sunnyside.booksToReadKMP

import io.sunnyside.common.bookstoreadkmp.*
import androidx.lifecycle.ViewModel
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.launch

class BookViewModel(bookRepository: BookRepository) : ViewModel() {
    val booksData = MutableLiveData<List<Book>>(emptyList())

    init {
        viewModelScope.launch {
            val books = bookRepository.fetchBooksFromRepo()
            booksData.value = books
        }
    }
}

