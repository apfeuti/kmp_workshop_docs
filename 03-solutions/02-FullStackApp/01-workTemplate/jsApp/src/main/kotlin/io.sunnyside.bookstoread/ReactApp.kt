package io.sunnyside.bookstoread

import io.sunnyside.common.bookstoreadkmp.Book
import react.*
import react.dom.*
import io.sunnyside.common.bookstoreadkmp.BookRepository
import io.sunnyside.common.bookstoreadkmp.fetchBooksFromRepoAsync
import kotlinx.coroutines.*
import kotlinx.html.js.onClickFunction


class Application : RComponent<ApplicationProps, RState>() {

    override fun RBuilder.render() {
        div(classes = "container") {
            div( "header clearfix") {
                h3 { +"Book Suggestions" }
            }
            child(bookList(), props = props)
        }

    }
}



fun bookList() = functionalComponent<ApplicationProps> { props ->
    val (books, setBooks) = useState(null as List<Book>?)
    val repository = BookRepository()

    fetchBooksFromRepoAsync(repository, success = {
        print ("Success")
    } ,error = {
        print ("error")
    })
    div {
        button(classes = "btn btn-primary") {
            +"Fetch"
            attrs {
                onClickFunction = {
                    val mainScope = MainScope()
                    mainScope.launch {
                        val books = repository.fetchBooksFromRepo()
                        setBooks(books)
                    }
                }
            }
        }

        books?.forEach {
            p {
                +"${it.title}: ${it.bookCategory}"
            }
        }
    }
}


external interface ApplicationProps : RProps {
}

fun RBuilder.app(handler: RHandler<ApplicationProps>) = child(Application::class, handler)

