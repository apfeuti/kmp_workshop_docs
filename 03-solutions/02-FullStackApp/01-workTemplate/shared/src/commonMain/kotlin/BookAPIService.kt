package io.sunnyside.common.bookstoreadkmp

import io.ktor.client.HttpClient
import io.ktor.client.features.json.JsonFeature
import io.ktor.client.features.json.serializer.KotlinxSerializer
import io.ktor.client.request.get
import io.ktor.client.request.url
import kotlinx.serialization.Serializable
import kotlinx.serialization.json.Json
import kotlinx.serialization.list
import io.ktor.client.request.HttpRequestBuilder
import io.ktor.client.request.header
import io.ktor.http.takeFrom

class BookAPIService {
    private val client: HttpClient = HttpClient {
        install(JsonFeature) {
            serializer = KotlinxSerializer(Json.nonstrict).apply {
                setMapper(Book::class, Book.serializer())
            }
        }

    }

    private val client2: HttpClient = HttpClient {

    }

    private fun HttpRequestBuilder.apiUrl(path: String) {
        url {
            // because of emulator localhost use the Host IP Adress ( run ifconfig)
            //takeFrom("http://localhost:3010/")
            //takeFrom("http://192.168.15.242:3010/")
            takeFrom("http://192.168.1.134:3010/")
            encodedPath = path
        }
    }



    suspend fun fetchBooks():List<Book> {
        val jsonArrayString = client.get<List<Book>> {
            apiUrl(path = "books")
        }
        return jsonArrayString

    }

    suspend fun fetchBooksAsString():String {
        val resultString = client2.get<String> {
            apiUrl(path = "books")
        }
        print (resultString)
        return resultString
    }
}