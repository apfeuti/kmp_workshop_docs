package io.sunnyside.common.bookstoreadkmp


import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.serialization.json.JsonArrayBuilder
import kotlinx.serialization.json.JsonObject
import kotlinx.serialization.json.JsonPrimitive

import kotlinx.serialization.json.jsonArray


class BookRepository()  {
    private val bookAPIService: BookAPIService = BookAPIService()

    suspend fun fetchBooksFromRepo(): List<Book>  {
        val result = bookAPIService.fetchBooks()
        val bookList = mutableListOf<Book>()
        result.forEach {
            bookList.add(it)
        }
        return bookList
    }

    suspend fun fetchBooksFromRepoAsString(): String  {
        val result = bookAPIService.fetchBooksAsString()
        return result
    }

    // Workaround iOS , a direct Call of "suspend fun fetchBooks()" in iOS would cause an error "there is no event loop, runBlocking", use "non suspending" method  which launches a predefined kotlinx.coroutine scope and delegates the call to the original suspending function


    fun fetchBooksFromRepo(success: (List<Book>) -> Unit) {
        GlobalScope.launch(Dispatchers.Main) {
            success(fetchBooksFromRepo())
        }
    }

}

// Workaround JS:
// Direct Call of "suspend fun fetchBooks()" from native Javascript is very complicated. This JS-freindly method with callbacks is provided and delegates to the original "suspend fun fetchBooks()" method

fun fetchBooksFromRepoAsync(obj: BookRepository , success: (String) -> Unit, error: (String) -> Unit) {
    GlobalScope.launch {
        try {
            success(obj.fetchBooksFromRepoAsString())
        } catch (e: Throwable) {
            error("fetchBooksFromRepoAsync error:" + e)
        }
    }
}