import io.sunnyside.common.bookstoreadkmp.BookRepository
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlinx.coroutines.runBlocking
import io.ktor.client.HttpClient

open class BookAPIServiceTest{
    @ExperimentalStdlibApi
    @Test
    open fun countBooksTest(){
        runBlocking {
            val bookRepository: BookRepository = BookRepository()
            val list = bookRepository.fetchBooksFromRepo()
            assertEquals(
                    7,
                    list.count()
            )
        }

    }

}